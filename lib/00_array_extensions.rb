# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    return self.reduce(:+) unless self.empty?
    return 0
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array
  def square!
    self.map! { |el| el * el }
  end

  def square
    self.map { |el| el * el }
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    result_array = []
    self.each do |el|
      if result_array.include?(el) == false
        result_array << el
      end
    end
    result_array
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)


class Array
  def two_sum
    #result array that keeps track of a pair of indexes that have elements that sum to zero (worry about order later
    #going to need two each methods for this one to check one element against the rest then shuffle into result array

    result_array = []
    self.each_with_index do |el, idx|
      self.each_with_index do |el_2, idx_2|
        next if idx_2 <= idx
        if el + el_2 == 0
          result_array << [idx, idx_2]
        end
      end
    end
    result_array

  end
end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array
  def median

    total = self.count
    median = (total / 2).to_f
    self.sort!

    if self.empty?
      return nil
    end

    if total.odd?
      return self [(median)]
    end
    if total.even?
      return ( ( ( self[(median) - 1].to_f ) + ( self[(median)].to_f ) ) / 2)
    end
  end
end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

class Array
  def my_transpose
    #okay so... we will be given an array with arrays.. we want to create a new array with arrays that take the corresponding index of each of the orginal arrays to create the new arrays...fuck
    result_array = []

    self.each_with_index do |el, idx|
      sub_array = []
      result_array << sub_array
      sub_array.clear
      self.each do |array|
        sub_array << array[idx]
      end
    end
    result_array

  end
end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).



class Array
  def my_transpose_rectangular
    matrices = self[0].count
    index = 0
    result_array = []
    while matrices > index
      sub_array = []
      result_array << sub_array
      sub_array.clear

      self.each do |array|
        sub_array << array[index]
      end
      index += 1
    end
    result_array
  end
end
